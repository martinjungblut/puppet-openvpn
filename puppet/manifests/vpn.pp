$openvpn_conf_template = "
ca   /var/lib/puppet/ssl/certs/ca.pem
cert /var/lib/puppet/ssl/certs/<%=@fqdn%>.pem
key  /var/lib/puppet/ssl/private_keys/<%=@fqdn%>.pem

client
remote 10.1.1.21 1194
dev tap
persist-key
persist-tun

comp-lzo

log-append /var/log/openvpn.log
status /var/log/openvpn-status.log
verb 3
writepid /var/run/openvpn.pid

group nogroup
user nobody
"

node debian-browser {
  schedule { 'daily':
    period => 'daily',
    repeat => 1,
  }

  exec { 'apt-get update':
    command  => 'apt-get update',
    schedule => 'daily',
    user     => 'root',
  }

  package { 'openvpn':
    ensure  => 'latest',
    require => Exec['apt-get update'],
  }

  file { '/etc/openvpn/openvpn.conf':
    content => inline_template($openvpn_conf_template),
    ensure  => 'present',
    group   => 'root',
    mode    => '600',
    notify  => Service['openvpn'],
    owner   => 'root',
    require => Package['openvpn'],
  }

  file { "/var/lib/puppet/ssl/certs/${$::fqdn}.pem":
    audit  => 'content',
    notify => Exec['restart-openvpn'],
  }

  exec { 'restart-openvpn':
    command     => '/bin/systemctl restart openvpn',
    refreshonly => true,
    unless      => '/bin/ping -c 3 10.8.0.1',
    user        => 'root',
  }

  service { 'openvpn':
    enable  => true,
    ensure  => 'running',
    require => Package['openvpn'],
  }
}
